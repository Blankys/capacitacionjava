/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.servicios;

import com.helpdesk.capacitacionjava.dao.UsuarioDao;
import com.helpdesk.capacitacionjava.modelos.Usuarios;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author David Ramirez
 */
@Stateless
public class UsuarioServiceImpl implements UsuarioService {
    @EJB
    UsuarioDao dao;

    @Override
    public List<Usuarios> listaUsuarios() {
       return dao.listaUsuarios();
    }

    @Override
    public void guardarUsuario(Usuarios usr) {
      dao.guardarUsuario(usr);
    }

    @Override
    public void editarUsuario(Usuarios usr) {
      dao.editarUsuario(usr);
    }

    @Override
    public Usuarios buscarUsuario(Integer usur_id) {
      return dao.buscarUsuario(usur_id);
    }

    @Override
    public void eliminarUsuario(Usuarios usuario) {
      dao.eliminarUsuario(usuario);
    }
    
    
}
